#!/bin/bash
#SBATCH --mem=16G
#SBATCH --cpus-per-task=16
#SBATCH --time=0-03:00      # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --mail-type=ALL
#SBATCH --mail-user=shanewidanagama@trentu.ca

#exports path variable to the output and NUCmer directory
export PATH=/home/shane88/projects/rrg-shaferab/shane88/tlatifolia/Quickmerge_Assemblies/t8_canu+merged:/home/shane88/projects/rrg-shaferab/shane88/tlatifolia/Quickmerge_Assemblies/t8_canu+merged/MUMmer3.23:$PATH

#aligns initial merged assembly to canu assembly
nucmer -l 100 -prefix out  ../t6_d2c+canu/merged_canu+dbg2olc.fasta canu_no_white.fasta
delta-filter -r -q -l 10000 out.delta > out.rq.delta2
#runs quickmerge with the initial merged assembly (DBG2OLC + Canu) as the query assembly and Canu assembly as the reference assembly
#with 5.0 anchor contig overlap cutoff, 1.5 overlap cutoff of contigs extending anchor contig, 8706000 bp length cutoff of anchor cutoff (based on N50 of reference assembly), and  20000 bp minimum merging length 
quickmerge -d out.rq.delta2 -q ../t6_d2c+canu/merged_canu+dbg2olc.fasta -r canu_no_white.fasta -hco 5.0 -c 1.5 -l 8706000 -ml 20000 -p canu+dbg2olc2
