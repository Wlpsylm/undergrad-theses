library(dplyr)
library(DBI)
library(RSQLite)
source("findcRMP.R")
source("popSimulation.R")
source("recombineFunction.R")
source("findFreqs.R")
options(scipen=999)

fifty <- dbConnect(RSQLite::SQLite(), "50gen.db")
ten <- dbConnect(RSQLite::SQLite(), "10gen.db")
hundred <- dbConnect(RSQLite::SQLite(), "thirdRun.db")
numLoci <- c(10, 50, 100, 1000, 5000)
Ns <- c(100, 1000, 10000, 100000)
strToVec <- function(x) { as.numeric( unlist(strsplit(x, split = "")) ) }


###graphing

#####10##########
results10 <- as.data.frame(matrix(nrow = 100, ncol = length(numLoci)*length(Ns)))
counter <- 1
G <- 10 #generations

for(n in Ns) { #N
  for(snp in numLoci) {
    # person 1  11010101
    # person 1  11110101
    # person 2  01100101
    # etc
    colnames(results10)[counter] <- paste(snp, "SNPs", n, "N", sep = "_")
    
    for (i in 1:100){
      indiv <- sample(1:n, 1)
      
      homo1 <- (2*indiv)-1
      homo2 <- 2*indiv
      
      randomPerson <- dbGetQuery(ten, paste0("SELECT * FROM population_SNP", snp,
                                               "_N", n,
                                               " WHERE rowid IN (", homo1, ", ", homo2, ")"))
      personGenome <- apply(randomPerson, MARGIN = 1, FUN = function(x){ strToVec(x)})
      
      freq <- dbGetQuery(ten, 
                         paste("SELECT Generation_", G, " FROM allele_frequency_table_SNP",
                               snp,"_N", n, sep = ""))
      
      results10[i, counter] <- findcRMP(as.vector(freq[,1]), personGenome)
    }
    
    counter <- counter + 1 
    
  } #end SNPs
} #end Ns

means10 <- apply(results10, MARGIN = 2, mean)
sds10 <- apply(results10, MARGIN = 2, sd)
means10[means10 < 1e-60] <- 1e-60
sds10[sds10 < 1e-60] <- 1e-60

t <- data.frame(x = 1:length(means10), 
                meansL = -log10(means10), 
                meansO = means10,
                sdsL = -log10(sds10),
                sdsO = sds10, 
                N = rep(Ns, each = 5), 
                SNP = factor(as.character(rep(numLoci, 4)), levels = c(10, 50, 100, 1000, 5000)),
                gen = as.character(rep(10, length(means10))))


######50#########
results50 <- as.data.frame(matrix(nrow = 100, ncol = length(numLoci)*length(Ns)))
counter <- 1
G <- 50 #generations

for(n in Ns) { #N
  for(snp in numLoci) {
    # person 1  11010101
    # person 1  11110101
    # person 2  01100101
    # etc
    colnames(results50)[counter] <- paste(snp, "SNPs", n, "N", sep = "_")
    
    for (i in 1:100){
      indiv <- sample(1:n, 1)
      
      homo1 <- (2*indiv)-1
      homo2 <- 2*indiv
      
      randomPerson <- dbGetQuery(fifty, paste0("SELECT * FROM population_SNP", snp,
                                               "_N", n,
                                               " WHERE rowid IN (", homo1, ", ", homo2, ")"))
      personGenome <- apply(randomPerson, MARGIN = 1, FUN = function(x){ strToVec(x)})
      
      freq <- dbGetQuery(fifty, 
                         paste("SELECT Generation_", G, " FROM allele_frequency_table_SNP",
                               snp,"_N", n, sep = ""))
      
      results50[i, counter] <- findcRMP(as.vector(freq[,1]), personGenome)
    }
    
    counter <- counter + 1 
    
  } #end SNPs
} #end Ns

means50 <- apply(results50, MARGIN = 2, mean)
sds50 <- apply(results50, MARGIN = 2, sd)
means50[means50 < 1e-60] <- 1e-60
sds50[sds50 < 1e-60] <- 1e-60

f <- data.frame(x = 1:length(means50), 
                meansL = -log10(means50), 
                meansO = means50,
                sdsL = -log10(sds50),
                sdsO = sds50, 
                N = rep(Ns, each = 5), 
                SNP = factor(as.character(rep(numLoci, 4)), levels = c(10, 50, 100, 1000, 5000)),
                gen = as.character(rep(50, length(means50))))


#########100############
results100 <- as.data.frame(matrix(nrow = 100, ncol = length(numLoci)*length(Ns)))
counter <- 1
G <- 100 #generations

for(n in Ns) { #N
  for(snp in numLoci) {
    # person 1  11010101
    # person 1  11110101
    # person 2  01100101
    # etc
    colnames(results100)[counter] <- paste(snp, "SNPs", n, "N", sep = "_")
    
    for (i in 1:100){
      indiv <- sample(1:n, 1)
      
      homo1 <- (2*indiv)-1
      homo2 <- 2*indiv
      
      randomPerson <- dbGetQuery(hundred, paste0("SELECT * FROM population_SNP", snp,
                                               "_N", n,
                                               " WHERE rowid IN (", homo1, ", ", homo2, ")"))
      personGenome <- apply(randomPerson, MARGIN = 1, FUN = function(x){ strToVec(x)})
      
      freq <- dbGetQuery(hundred, 
                         paste("SELECT Generation_", G, " FROM allele_frequency_table_SNP",
                               snp,"_N", n, sep = ""))
      
      results100[i, counter] <- findcRMP(as.vector(freq[,1]), personGenome)
    }
    
    counter <- counter + 1 
    
  } #end SNPs
} #end Ns

means100 <- apply(results100, MARGIN = 2, mean)
sds100 <- apply(results100, MARGIN = 2, sd)
means100[means100 < 1e-60] <- 1e-60
sds100[sds100 < 1e-60] <- 1e-60

h <- data.frame(x = 1:length(means100), 
                meansL = -log10(means100), 
                meansO = means100,
                sdsL = -log10(sds100),
                sdsO = sds100, 
                N = rep(Ns, each = 5), 
                SNP = factor(as.character(rep(numLoci, 4)), levels = c(10, 50, 100, 1000, 5000)),
                gen = as.character(rep(100, length(means100))))





r <- rbind(t, f, h)
r$gen <- factor(r$gen, levels = c(10, 50, 100))


#######PLOTING#######
k <- ggplot(data = r, mapping = aes(x = log10(N), y = meansL, ymax = 60, xmin = 1, xmax = 6))
p <- k + geom_point(size = 2, aes(colour = gen, shape = gen))  +
  labs(colour = "Generation", shape = "Generation") +
  facet_grid(.~SNP) + 
  geom_hline(yintercept = -log10(1/100), linetype="dashed", size = 0.2) +
  # geom_hline(yintercept = -log10(1/1000), linetype="dashed", size = 0.2) +
  # geom_hline(yintercept = -log10(1/1000000), linetype="dashed", size = 0.2) +
  # geom_hline(yintercept = -log10(1/1000000000), linetype="dashed", size = 0.2) +
  geom_hline(yintercept = -log10(1/100000000000000), linetype="dashed", size = 0.2) +
  geom_text(x = 3.5, y = 1, label = "1 in 100", size = 4) +
  geom_text(x=3.5, y=13, label="1 in 100 trillion", size = 4) +
  xlab("log N") +
  ylab("-log mean cRMP") +
  ggtitle("Loci vs N") +
  theme_light() 

# #+ theme(axis.text.x = element_text(angle = 90, hjust = 1))
p
