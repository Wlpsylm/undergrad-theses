#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=stats
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --cpus-per-task=32
#SBATCH --mem-per-cpu=4G
#SBATCH --time=0-09:59:00 # time (DD-HH:MM)
#SBATCH -o %x-%j.log

module load StdEnv/2020 gcc/9.3.0
module load samtools/1.13
module load bcftools/1.13

mkdir coverage2 depth2

for i in $(ls *.unique_reads.bam | sed 's/.bam//')
do
samtools coverage --min-BQ 20 --min-MQ 20 -o ./coverage2/$i.tsv $i\.bam
done

for i in $(ls *.unique_reads.bam | sed 's/.bam//')
do
samtools depth $i.bam | awk '{total += $3 } END { print total/NR }' > ./depth2/$i.txt
done

cd ./coverage2/

for i in $(ls *.tsv | sed 's/.tsv//')
do
awk '{print $5}' $i.tsv > $i.tmp
done
paste *tmp > covered_bases.txt
rm -rf *tmp

for i in $(ls *.tsv | sed 's/.tsv//')
do
awk '{print $7}' $i.tsv > $i.tmp
done
paste *tmp > depth.txt
rm -rf *tmp

for i in $(ls *.tsv | sed 's/.tsv//')
do
awk '{print $8}' $i.tsv > $i.tmp
done
paste *tmp > baseQ.txt
rm -rf *tmp

for i in $(ls *.tsv | sed 's/.tsv//')
do
awk '{print $9}' $i.tsv > $i.tmp
done
paste *tmp > mapQ.txt
rm -rf *tmp

for i in $(ls *.tsv | sed 's/.tsv//')
do
awk '{print $6}' $i.tsv > $i.tmp
done
paste *tmp > coverage.txt
rm -rf *tmp

cd ./../
cd ./depth2/
cat *txt > whole_depths.txt
#END

#To average coverage of samples in coverage.txt
awk 'NR==1;NR>1{for (i=1;i<=NF;i++){a[i]+=$i}}END{for (i=1;i<=NF;i++){printf a[i]/(NR-1)" "};printf "\n"}' coverage.txt

