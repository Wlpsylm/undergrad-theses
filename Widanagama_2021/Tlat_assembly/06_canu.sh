#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=canu_mm_mt
#SBATCH --time=0-01:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module load gcc/7.3.0
module load canu/2.0

#make contigs from pacbio long reads with 257Mb estimated genome size, 124 GB max memory, max thread number of 32
#and 32 GB allocated to the Canu task executive. Scheduler options ask for 32 threads, and 1 hour of time allocated
canu \
  -p tl -d tl-pacbio \
  genomeSize=257m \
  -pacbio  tlgenome.fastq \
  -maxMemory=124G \
  -maxThreads=32 \
  -executiveMemory=32 \
  gridOptions="--account=rrg-shaferab --cpus-per-task=32 --time=1:00:00"
