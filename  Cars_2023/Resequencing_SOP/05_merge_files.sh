#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=merge
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=0-4:59 # time (DD-HH:MM)

module load samtools

samtools merge Odo_Key3_S_L_merged.sorted_reads.bam Odo_Key3_S3_L001.sorted_reads.bam Odo_Key3_S5_L007.sorted_reads.bam
samtools merge Odo_Key5_S_L_merged.sorted_reads.bam Odo_Key5_S5_L001.sorted_reads.bam Odo_Key5_S6_L007.sorted_reads.bam
samtools merge Odo_QC2_S_L_merged.sorted_reads.bam Odo_QC2_S5_L001.sorted_reads.bam Odo_QC2_S6_L001.sorted_reads.bam
samtools merge Odo_ON6_S_L_merged.sorted_reads.bam Odo_ON6_S12_L008.sorted_reads.bam Odo_ON6_S1_L001.sorted_reads.bam
