#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=abss_ctg_32_125
#SBATCH --cpus-per-task=32
#SBATCH --mem=125G
#SBATCH --time=00-48:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)

module load gcc/5.4.0
module load abyss/2.2.4

#run paired-end abyss with a kmer length 101 using 20% of raw PE Illumina reads to generate contigs 
abyss-pe name=tl k=101 in='tlatifolia_20pct_raw_1.fastq tlatifolia_20pct_raw_2.fastq'
