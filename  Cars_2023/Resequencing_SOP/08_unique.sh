#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=12G
#SBATCH --time=0-03:00 # time (DD-HH:MM)

module load samtools
module load sambamba
echo ${1}
/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/sambamba-0.8.2-linux-amd64-static view \
--nthreads=30 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.deduped_reads.bam \
-o /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.unique_reads.bam &&
/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/sambamba-0.8.2-linux-amd64-static flagstat \
--nthreads=30 \
/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.unique_reads.bam \
> /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.unique_reads.flagstat
