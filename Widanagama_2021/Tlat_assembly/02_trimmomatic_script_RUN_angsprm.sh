#listing unique files and takes first half of them
for f in $(ls *.fastq | cut -f1 -d'.'| uniq)
do
#prints name of file being trimmed
echo ${f}
#tells slurm to output slurm script in that directory and how it is named 
sbatch -o ~/projects/rrg-shaferab/shane88/tlatifolia/trimmed_slurm/${f}-%A.out 02_trimmomaticPE_angsprm.sh ${f}
#wait for ten seconds
sleep 10
#indicates end of loop
done
