#make list of all edited bam files (bam_unique.list) 

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=angsd
#SBATCH --mem 88G
#SBATCH --cpus-per-task 10
#SBATCH --time=03-00:00
#SBATCH --output=%x-%j.out
module load angsd

angsd -bam bam_unique.list -doMajorMinor 1 -domaf 1 -out WTD.51.angsd -doBcf 1 -nThreads 5 -doGeno -4 -doPost 1 -gl 2 -SNP_pval 1e-6 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1 -doGlf 2
