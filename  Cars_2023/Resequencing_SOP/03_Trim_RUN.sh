mkdir trimmed_slurm
for f in $(ls *.fastq.gz | cut -f1-4 -d'_'| uniq) #for Odo_ON_X samples cut -f1-5 -d'_'
do
echo ${f}
sbatch -o /home/bcars/projects/def-shaferab/bcars/thesis/trimmed_slurm/${f}-%A.out 03_Trim.sh ${f}
sleep 10
done
