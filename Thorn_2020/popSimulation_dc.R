library(hypred)

# you need better variable names
# eg G = num_gen
#    N = pop_size  -- change #1

# Change #1
# Instead of having a fixed N, we are going to have 
# pop_size be a vector of length num_gen OR
# a single number
#

# NOTE: in your help file, num_gen will be the number of integenerational transitions, not 
# technically the number of total generations observed (essentially, num_gen = 100 means we stop
# at 99 - sort-of)
popSimulation <- function(num_gen = 2, 
                          pop_size = 100, 
                          mutRate, 
                          genome.ref,
                          initial_pop = NULL){
  
  # code that checks to see the class/is.vector/length of pop_size
  stopifnot(class(pop_size) == "numeric", is.vector(pop_size))
  if(length(pop_size) == 1) {
   pop_size <- rep(pop_size, num_gen)
  } else if(length(pop_size) > 1 & length(pop_size) != num_gen) {
    stop("Argument pop_size must be either 1 number or num_gen numbers.")
  }
  
  if(!is.null(initial_pop)) {
    stopifnot(dim(initial_pop)[2] == pop_size * 2)
    random.mate.pop <- initial_pop  
  } else {
    # This is the null case - if not specified, initialize population to use
    # homologues that are perfect 1/0 hetero for every member of the population,
    # no frequency distributions at all
    random.mate.pop <- matrix(nrow = loci, ncol = pop_size[1] * 2)
    
    # starter generation - can only get 1 from one parent and 0 from other 
    # so will be 1/0 at all loci
    random.mate.pop[ , seq(1, pop_size[1] * 2, by = 2)] <- 1
    random.mate.pop[ , seq(2, pop_size[1] * 2, by = 2)] <- 0
  }
  
  random.mate.pop.temp <- random.mate.pop
  
  # holds 1 frequencies for each gen
  propExpressedPerGen <- as.data.frame(matrix(nrow = loci, ncol = num_gen-1)) 
  
  colnames(propExpressedPerGen) <- paste(rep("Generation", ncol(propExpressedPerGen)), 
                                         1:(ncol(propExpressedPerGen)), 
                                         sep = "_") #names the columns

  #pdf(paste("Plots/improved_allele_frequcy_trial_", trialnum, ".pdf", sep = ""))
  
  for (generation in 1:(num_gen-1)) {
    ## loop over generations
    pop_for_gen <- pop_size[generation]
    # make this the correct size ... it gets overwritten below in the loop anyway
    random.mate.pop.temp <- random.mate.pop
    
    for (indiv in 1:pop_for_gen) {
      ## loop over individuals
      ## in order to keep the population stable, each parent pair will have 2 offspring
      ## for this to work, each parent needs to produce 2 gametes
      
      ## gamete 1
      random.mate.pop.temp[ ,(2 * indiv - 1)] <-
        recombine(genome.ref,
                  random.mate.pop[ ,(2 * indiv - 1)],
                  random.mate.pop[ ,(2 * indiv - 0)],
                  mutRate)
      ## gamete 2
      random.mate.pop.temp[ ,(2 * indiv-0)] <-
        recombine(genome.ref, 
                  random.mate.pop[ ,(2 * indiv - 1)] , 
                  random.mate.pop[ ,(2 * indiv - 0)], 
                  mutRate)
      
    } ## end for N - individuals
    
    ## permutate - bumpin' uglies
    ## replace is false so no offspring can get the same gamete for both halfs of chromosome
    ## each offspring "chooses" their 2 parent gametes - consistent with W-F models
    random.mate.pop <- random.mate.pop.temp[ , sample(1:(pop_for_gen * 2), pop_size[generation + 1] * 2, replace = TRUE)]
    # now: random.mate.pop is the correct size - that is, 2 * pop_size[generation + 1] cols
    
    # at this point, we have a new population
    propExpressedPerGen[ , generation] <- findFreqs(random.mate.pop)
    
  } ## end for num_gen
  
  # adds gen0 (ie first heterozygotes)
  propExpressedPerGen <- as.data.frame(Generation_0 = rep(0.5, loci), propExpressedPerGen) 
  values <- list(as.data.frame(random.mate.pop), propExpressedPerGen)
  return(values)
}

