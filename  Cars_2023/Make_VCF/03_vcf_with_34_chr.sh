# Exctract 34 relevant chromsomes from vcf
 module load bioawk
 data=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam
ref=/home/bcars/projects/def-shaferab/bcars/thesis
vcf=$data/WTD.51.angsd.vcf
 bioawk -c fastx -t '{print $name, length($seq)}' $ref/chr_odocoileus_virginianus_NEW.fa | sort -k2 -n | tail -n 35 | cut -f 1 > 34_chr.txt

#Make file 03_vcf_split.sh to split vcf
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=vcf_split

data=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam
ref=/home/bcars/projects/def-shaferab/bcars/thesis
vcf=$data/WTD.51.angsd.vcf

grep "^#" ${1} > vcf_header

while read line
do
grep -v "^#" ${1} | awk -v scaff=$line '{ if ( $1==scaff ) print }' | cat vcf_header - > vcf_chr/$line.vcf
done < 34_chr.txt

#copy and paste into terminal:
 # split vcf
 data=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam
 ref=/home/bcars/projects/def-shaferab/bcars/thesis
 mkdir vcf_chr
 vcf=WTD.51.angsd.vcf
 sbatch -o vcf_split-%A.out --cpus-per-task 8 --mem=2G --time=16:00:00 03_vcf_split.sh ${vcf}

#Zip files and make indices
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=tabix
#SBATCH --cpus-per-task=1
#SBATCH --mem=1G
#SBATCH --time=0-03:00 # time (DD-HH:MM)
 module load tabix
for f in `ls *.vcf`
do
bgzip $f
done

for f in `ls *.vcf.gz`
do
tabix -p vcf $f
done
#END

# Assemble vcf of 34 chromosomes
module load vcftools
vcf-concat chr_10.vcf.gz chr_15.vcf.gz chr_1.vcf.gz chr_24.vcf.gz chr_29.vcf.gz chr_33.vcf.gz chr_6.vcf.gz chr_11.vcf.gz chr_16.vcf.gz chr_20.vcf.gz chr_25.vcf.gz chr_2.vcf.gz chr_34.vcf.gz chr_7.vcf.gz chr_12.vcf.gz chr_17.vcf.gz chr_21.vcf.gz chr_26.vcf.gz chr_30.vcf.gz chr_3.vcf.gz chr_8.vcf.gz chr_13.vcf.gz chr_18.vcf.gz chr_22.vcf.gz chr_27.vcf.gz chr_31.vcf.gz chr_4.vcf.gz chr_9.vcf.gz chr_14.vcf.gz chr_19.vcf.gz chr_23.vcf.gz chr_28.vcf.gz chr_32.vcf.gz chr_5.vcf.gz > 34_chr.vcf.gz
