#!/bin/bash
#SBATCH --job-name=fastqc
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=16
#SBATCH --mem=8G
#SBATCH --time=0-08:00 # time (DD-HH:MM)

module load fastqc

for f in `ls *_R1_001.fastq.gz`
do
fastqc $f
done
