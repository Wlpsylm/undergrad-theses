#!/bin/bash
#SBATCH --time=0-16:00
#SBATCH --account=rrg-shaferab
#SBATCH --mem=8G
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=ALL
#module load samtools
module load StdEnv/2020
module load  gcc/9.3.0 boost/1.72.0

#convert raw subreads from bam to fastq file
bam2fastq -o tlgenome m54204U_200925_191208.subreads.bam

#alternatively you can use this
#module load samtools/1.12
#samtools fastq m54204U_200925_191208.subreads.bam > tl_subreads.fq
