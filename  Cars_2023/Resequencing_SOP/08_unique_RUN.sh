#cd /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam

mkdir unique_slurm
for f in $(ls *.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/unique_slurm/${f}-%A.out 10_unique.sh ${f}
sleep 10
done
