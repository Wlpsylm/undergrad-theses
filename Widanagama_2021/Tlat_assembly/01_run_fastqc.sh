#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=pltns_contig2
##--cpus-per-task=16
#SBATCH --mem=32G
#SBATCH --time=0-8:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)
./../angsrm_pipeline/01_fastqc.bash
