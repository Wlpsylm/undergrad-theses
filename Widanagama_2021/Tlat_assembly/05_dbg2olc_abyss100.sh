#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=d2c
#SBATCH --cpus-per-task=17
#SBATCH --mem=200G
#SBATCH --time=0-24:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)

#after downloading and building ('make' command) DBG2OLC source code
#make contigs with 17 threads, adaptive threshold 0.001, Kmer coverage threshold 2, minimum overlap of 20, RemoveChimera set to true
#and outputs STDOUT to log file
#uses the ABySS contigs made from 100% of Illumina reads
./DBG2OLC k 17 AdaptiveTh 0.001 KmerCovTh 2 MinOverlap 20 RemoveChimera 1 Contigs tl-6.fa f tlgenome.fastq > DBG2OLC.log
