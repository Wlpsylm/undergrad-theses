
options(scipen=999)

popdb <- dbConnect(RSQLite::SQLite(), "moreSNP.db")
## dbs are thirdRun (G = 100) moreSNP(higher res) 10gen 50gen
numLoci <- c(10, 30, 50, 70, 90, 110)
Ns <- c(100, 1000, 10000, 100000)

    plot(x = 1:100, y = rep(0,100), 
         ylim = c(0,0.2), 
         col = rgb(0, 0, 0, alpha = 0),
         xlab = "Generation",
         ylab = "Proportion of Fixed (or lost) Loci",
         main = "Loci loss over time")
         #main = paste("N:", n, ", Loci loss over time (SNP=", snp, ")", sep = "")) 
    
par(mfrow = c(1,1))

 
gone <- data.frame(loss = 1:2400, n = 1:2400, snp = 1:2400, gen = rep(1:100, 24))
counter = 1
for(n in Ns){
  for(snp in numLoci){
    population <- dbGetQuery(popdb, paste("SELECT * FROM allele_frequency_table_SNP",snp, 
                                         "_N", n, sep = "")) 
    gone[counter:(counter+99), 1] <- apply(population, 
                                          MAR = 2, 
                                          FUN = function(x) length(which(x == 1 | x == 0))/(nrow(population)))
    gone[counter:(counter+99), 2] <- rep(n, 100)
    gone[counter:(counter+99), 3] <- rep(snp, 100)
  
    counter <- counter+100
    
    }
    
}


ggplot(data = gone, aes(x = gen, y = loss
                        #, color = as.factor(snp)
                        ))+
  geom_line()+
  #facet_grid(rows = vars(n), labeller = label_both)+
  facet_grid(snp~n, labeller = label_both) +
  ggtitle("Loci Loss Over Time")+
  xlab("Generation")+
  ylab("Proportion Lost") + 
  labs(color = "Number of SNPs")


