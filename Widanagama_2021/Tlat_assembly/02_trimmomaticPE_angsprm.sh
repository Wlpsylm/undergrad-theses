#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic
#SBATCH --cpus-per-task=16
#SBATCH --mem=130G
#SBATCH --time=0-06:00 # time (DD-HH:MM)

# load trimmomatic module
module load trimmomatic

#provides trimmomatics input and output file names and configuration
java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE \
-threads 16 -phred33 ${1}_1.fastq ${1}_2.fastq  ${1}_1P.fastq ${1}_1PNA.fastq ${1}_2P.fastq ${1}_2PNA.fastq \
ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
