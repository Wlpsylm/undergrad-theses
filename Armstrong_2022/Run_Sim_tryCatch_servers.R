current_fname <- "results_500_0.1_55-45.rdata"
NOC = 2
trials = 1000
mu = 500
sigma = 0.1
mx = c(0.55, 0.45)


#
#  do_parallel
#
#  Setup data, and organize things to run parallel iterations for
#  simulations of EuroForMix, by parameter type
#
#  Note: brute-forces the parameters, parallelizes the iterations (for now)
#
#

######### Set-Up     ########
library("euroformix")
popfn  <- "~/ESX17_Norway.csv" # location of allele frequency database
popFreq <- freqImport(popfn)[[1]]   # obtain list with population frequencies

# find kit name - must be specified in file title
for (i in getKit()) {
  if (length(grep(i, popfn)) > 0)
    kit <- i
}

######### Source Functions for Use ########
source("~/parallel_fns.R")

######### Results     ########


################################################################################
#
#  Run Simulation X - CC#1: 
#  * NOC = 2
#  * trials = 1000
#  * mu = 1500            Range: 100-5000 RFU
#  * sigma = 0.5          Range: 0.1-0.6
#  * mx = 60% / 40%        Range: c(0.50-1.00, 0.01-0.50)
#
################################################################################


profile <- NULL
tmp <- NULL

ind_node <- function(NOC, popFreq, mx, mu, sigma, kit, seed, maxThreads) {
  library("euroformix")
  tmp <- tryCatch({
    run_replicate(
      NOC,
      popFreq,
      mx,
      mu,
      sigma,
      kit,
      seed = seed,
      maxThreads = maxThreads
    )
  }, error = function(e) {
    paste("ERROR in trial with seed ", seed, ":   ", e, "\n") # Print seed and error message
  })
  tmp
}

#################################
#
#  doParallel and foreach setup
#
#################################
library(doParallel) 

# Use the environment variable SLURM_CPUS_PER_TASK to set the number of cores.
ncores = Sys.getenv("SLURM_CPUS_PER_TASK")  # should be ncores PER TASK

# Create an array from the NODESLIST environnement variable
nodeslist = unlist(strsplit(Sys.getenv("NODESLIST"), split=" "))

# Create the cluster with the nodes name. One process per count of node name.
# nodeslist = node1 node1 node2 node2, means we are starting 2 processes on node1, likewise on node2.
cl = makeCluster(nodeslist, type = "PSOCK") 
registerDoParallel(cl)

#registerDoParallel(cores=ncores)# Shows the number of Parallel Workers to be used
#print(ncores) # this how many cores are available, and how many you have requested.
#getDoParWorkers()# you can compare with the number of actual workers

# be careful! foreach() and %dopar% must be on the same line!
res <- foreach(seed=1:trials) %dopar% { 
  print(seed)
  ind_node(NOC, popFreq, mx, mu, sigma, kit, seed, maxThreads = ncores) 
}
results_500_0.1_55 <- do.call("rbind", res)
# Don't forget to release resources
stopCluster(cl)

save(file = current_fname, results_500_0.1_55)

